/*
 * motor.c
 *
 *  Created on: 2022年4月7日
 *      Author: XXWY
 */
#include "zf_gtm_pwm.h"
#include "zf_gpio.h"
#include "move_filter.h"
#include "zf_stm_systick.h"
#include "SEEKFREE_FUN.h"
#include "pid.h"
#include "bldc_config.h"
#include "motor.h"

motor_struct motor_control;

uint8 now_dir=0;
uint32 commutation_time_save[6] = {COMMUTATION_TIMEOUT, COMMUTATION_TIMEOUT, COMMUTATION_TIMEOUT, COMMUTATION_TIMEOUT, COMMUTATION_TIMEOUT, COMMUTATION_TIMEOUT};  //换相时间 保存6次
uint32 commutation_time_sum = 0;    //六次换相时间总和
uint32 commutation_time = COMMUTATION_TIMEOUT;    //统计本次换相所需时间
uint8 phase_change=0;

uint8 motor_state;

//转速信息输出引脚
void motor_information_out_init(void)
{
    gtm_pwm_init(MOTOR_SPEED_OUT_PIN, 50, 0);       //初始化电机速度输出引脚，输出为频率变化的信号，例如电机转速为每分钟5000转，则引脚上的频率为5000Hz。
    gpio_init(MOTOR_DIR_OUT_PIN, GPO, 0, PUSHPULL); //初始化电机方向输出引脚（用户端接收）
}
//旋转方向改变 切换换相表
void advance_switch(void)
{
    GTM_SPE0_CTRL_STAT.B.SPE_EN   = FALSE;

    if(now_dir){
        GTM_SPE0_OUT_PAT0.U = OUTPUT_PAT_0;//设置当在对应的值达到的时候 控制TOM输出通道的状态
        GTM_SPE0_OUT_PAT1.U = OUTPUT_PAT_1;
        GTM_SPE0_OUT_PAT2.U = OUTPUT_PAT_2;
        GTM_SPE0_OUT_PAT3.U = OUTPUT_PAT_3;
        GTM_SPE0_OUT_PAT4.U = OUTPUT_PAT_4;
        GTM_SPE0_OUT_PAT5.U = OUTPUT_PAT_5;
    }
    else
    {
        GTM_SPE0_OUT_PAT0.U = OUTPUT_PAT_AD_0;//设置当在对应的值达到的时候 控制TOM输出通道的状态
        GTM_SPE0_OUT_PAT1.U = OUTPUT_PAT_AD_1;
        GTM_SPE0_OUT_PAT2.U = OUTPUT_PAT_AD_2;
        GTM_SPE0_OUT_PAT3.U = OUTPUT_PAT_AD_3;
        GTM_SPE0_OUT_PAT4.U = OUTPUT_PAT_AD_4;
        GTM_SPE0_OUT_PAT5.U = OUTPUT_PAT_AD_5;
    }
    GTM_SPE0_CTRL_STAT.B.SPE_EN   = TRUE;
}
//设置旋转方向并输出方向
void motor_set_dir(void)
{
    if(gpio_get(MOTOR_DIR_IN_PIN) != now_dir)
    {
        now_dir = gpio_get(MOTOR_DIR_IN_PIN);
        advance_switch();
    }
}

//占空比输出
void speed_ctrl(speed_st speed)
{
    /* Load duty and period */
    if( speed.duty > speed.period )
    {
        /* Do nothing */
    }
    else
    {
        GTM_TOM0_CH0_SR1.U = speed.duty     & 0xFFFF;
        GTM_TOM0_CH0_SR0.U = speed.period   & 0xFFFF;
    }
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      计算当前的速度
//  @param      void
//  @return     void
//  @since
//-------------------------------------------------------------------------------------------------------------------
inline void calc_speed(void)
{
    //转速计算
    uint8 i;
    int32 speed;
    commutation_time_sum = 0;
    for(i=0; i<6; i++)
    {
        commutation_time_sum += commutation_time_save[i];
    }

    //电机转速计算说明
    //2.commutation_time_sum是统计电机换相6次总共使用了多少时间
    //3.通常电机转速我们都用RPM表示，RPM表示每分钟电机的转速
    //3.电机转一圈需要换相的次数等于 电机极对数*6
    //4.因此电机转速等于60*主频/电机极对数/commutation_time_sum，这样可以得到电机每分钟的转速

    speed = FCY/POLEPAIRS/commutation_time_sum*60;

    if(REVERSE == motor_control.dir)//电机反转的时候需要对速度取反
    {
        speed = -speed;
    }
    move_filter_calc(&speed_filter, speed);

}
//换相处理 保存在上一相位等待的时间
void phase_change_manage(void)
{
    commutation_time = systick_getval(STM1);
    systick_start(STM1);

    phase_change++;
    if(phase_change >= 6)
    {
        phase_change = 0;
    }
    commutation_time_save[phase_change] = commutation_time;       //速度信息保存

    commutation_time=0;

    calc_speed();
}
//换相超时处理
void phase_change_time_check(void)
{
    if( systick_getval(STM1) > 5000000)
    {
        commutation_time = COMMUTATION_TIMEOUT;
        commutation_time_save[0] = COMMUTATION_TIMEOUT;
        commutation_time_save[1] = COMMUTATION_TIMEOUT;
        commutation_time_save[2] = COMMUTATION_TIMEOUT;
        commutation_time_save[3] = COMMUTATION_TIMEOUT;
        commutation_time_save[4] = COMMUTATION_TIMEOUT;
        commutation_time_save[5] = COMMUTATION_TIMEOUT;

        //滑动平均滤波初始化
        move_filter_init(&speed_filter);
        motor_state=MOTOR_STOP;
    }
    else
    {
        motor_state=MOTOR_RUN;
    }
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机转动方向输出
//  @param      void
//  @return     void
//  @since
//-------------------------------------------------------------------------------------------------------------------
void motor_dir_out(void)
{
    if(now_dir)
    {
        motor_control.dir = FORWARD;
    }
    else
    {
        motor_control.dir = REVERSE;
    }
    gpio_set(MOTOR_DIR_OUT_PIN, motor_control.dir);
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机速度输出
//  @param      void
//  @return     void
//  @since      每次换相的时候翻转IO，外部控制器用编码器接口直接采集即可
//              速度引脚连接外部控制器编码器接口的A通道 方向引脚连接B通道
//-------------------------------------------------------------------------------------------------------------------
void motor_speed_out(void)
{
    if(speed_filter.data_average)
    {
        pwm_frequency(MOTOR_SPEED_OUT_PIN, myabs(speed_filter.data_average), 5000);     //频率更改函数并未在开源库中写入 需要使用的同学自行将此函数更新到开源库
    }
    else
    {
        pwm_frequency(MOTOR_SPEED_OUT_PIN, 1000, 0);
    }
}

//初始化电机运行模式以及部分参数
//-------------------------------------------------------------------------------------------------------------------
//  @brief      电机参数初始化
//  @param      void
//  @return     void
//  @since
//-------------------------------------------------------------------------------------------------------------------
void motor_init(void)
{
    #if BLDC_BRAKE_ENABLE==1
        motor_control.brake_flag = 1;   //刹车使能
    #else
        motor_control.brake_flag = 0;   //刹车关闭
    #endif
    motor_control.dir = FORWARD;                    //设置默认的方向
    #if BLDC_CLOSE_LOOP_ENABLE
    motor_control.set_speed = 0;
    motor_control.max_speed = BLDC_MAX_SPEED;       //设置最大正转速度
    motor_control.min_speed = -BLDC_MAX_SPEED;      //设置最大反转速度
    #endif
}
