#include "motor.h"
#include "gtm_pwm.h"

spe_st spe_g;


void clock_cfg(void)
{
    IfxGtm_enable(&MODULE_GTM);

    GTM_CMU_CLK_EN.U                    = CMU_CLK_ALL_DISABLE;



    GTM_CMU_GCLK_NUM.B.GCLK_NUM         = 1;//分频系数的分子
    GTM_CMU_GCLK_NUM.B.GCLK_NUM         = 1;
    GTM_CMU_GCLK_DEN.B.GCLK_DEN         = 1;//分频系数的分母

    GTM_CMU_CLK_0_CTRL.B.CLK_CNT        = 0;//分频系数
    GTM_CMU_CLK_1_CTRL.B.CLK_CNT        = 0;
    GTM_CMU_FXCLK_CTRL.B.FXCLK_SEL      = 0;//选择FXCLK的时钟来源

    GTM_CMU_CLK_EN.B.EN_CLK0            = CMU_CLK_ENABLE;
    GTM_CMU_CLK_EN.B.EN_CLK1            = CMU_CLK_ENABLE;
    GTM_CMU_CLK_EN.B.EN_FXCLK           = CMU_CLK_ENABLE;
}

void ccm_cfg(void)
{
    GTM_CTRL.B.RF_PROT                  = TRUE;         //关闭全局中断 1：关闭
    GTM_CTRL.B.RF_PROT                  = FALSE;        //开启全局中断 0：开启
}

void hall_tim_input_cfg(void)
{
    IfxGtm_PinMap_setTimTin(&HALL_A, IfxPort_InputMode_pullUp);
    IfxGtm_PinMap_setTimTin(&HALL_B, IfxPort_InputMode_pullUp);
    IfxGtm_PinMap_setTimTin(&HALL_C, IfxPort_InputMode_pullUp);

    IfxGtm_PinMap_setTimTin(&FSOI_PIN, IfxPort_InputMode_pullDown);

    GTM_TIM0_CH0_CTRL.B.CICTRL = IfxGtm_Tim_Input_currentChannel;//使用当前通道
    GTM_TIM0_IN_SRC.B.MODE_0   = 0;
    GTM_TIM0_IN_SRC.B.VAL_0    = 0;

    GTM_TIM0_CH1_CTRL.B.CICTRL = IfxGtm_Tim_Input_currentChannel;//使用当前通道
    GTM_TIM0_IN_SRC.B.MODE_1   = 0;
    GTM_TIM0_IN_SRC.B.VAL_1    = 0;

    GTM_TIM0_CH2_CTRL.B.CICTRL = IfxGtm_Tim_Input_currentChannel;//使用当前通道
    GTM_TIM0_IN_SRC.B.MODE_2   = 0;
    GTM_TIM0_IN_SRC.B.VAL_2    = 0;


    GTM_TIM0_CH0_CTRL.B.CLK_SEL      = IfxGtm_Cmu_Clk_0;
    GTM_TIM0_CH0_CTRL.B.FLT_CNT_FRQ  = IfxGtm_Cmu_Tim_Filter_Clk_0;//滤波计数频率选择
    GTM_TIM0_CH0_CTRL.B.FLT_EN       = TRUE;//滤波使能
    GTM_TIM0_CH0_CTRL.B.FLT_MODE_FE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//下降沿滤波模式 立即
    GTM_TIM0_CH0_CTRL.B.FLT_MODE_RE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//上降沿滤波模式 立即
    GTM_TIM0_CH0_CTRL.B.FLT_CTR_FE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH0_CTRL.B.FLT_CTR_RE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH0_FLT_FE.B.FLT_FE     = FILTER_CON_NUM;//设置滤波计数值
    GTM_TIM0_CH0_FLT_RE.B.FLT_RE     = FILTER_CON_NUM;

    GTM_TIM0_CH1_CTRL.B.CLK_SEL      = IfxGtm_Cmu_Clk_0;
    GTM_TIM0_CH1_CTRL.B.FLT_CNT_FRQ  = IfxGtm_Cmu_Tim_Filter_Clk_0;//滤波计数频率选择
    GTM_TIM0_CH1_CTRL.B.FLT_EN       = TRUE;//滤波使能
    GTM_TIM0_CH1_CTRL.B.FLT_MODE_FE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//下降沿滤波模式 立即
    GTM_TIM0_CH1_CTRL.B.FLT_MODE_RE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//上降沿滤波模式 立即
    GTM_TIM0_CH1_CTRL.B.FLT_CTR_FE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH1_CTRL.B.FLT_CTR_RE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH1_FLT_FE.B.FLT_FE     = FILTER_CON_NUM;//设置滤波计数值
    GTM_TIM0_CH1_FLT_RE.B.FLT_RE     = FILTER_CON_NUM;

    GTM_TIM0_CH2_CTRL.B.CLK_SEL      = IfxGtm_Cmu_Clk_0;
    GTM_TIM0_CH2_CTRL.B.FLT_CNT_FRQ  = IfxGtm_Cmu_Tim_Filter_Clk_0;//滤波计数频率选择
    GTM_TIM0_CH2_CTRL.B.FLT_EN       = TRUE;//滤波使能
    GTM_TIM0_CH2_CTRL.B.FLT_MODE_FE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//下降沿滤波模式 立即
    GTM_TIM0_CH2_CTRL.B.FLT_MODE_RE  = IfxGtm_Tim_FilterMode_individualDeglitchTime;//上降沿滤波模式 立即
    GTM_TIM0_CH2_CTRL.B.FLT_CTR_FE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH2_CTRL.B.FLT_CTR_RE   = IfxGtm_Tim_FilterCounter_upDown;//这位需要FLT_MODE_RE设置为1才有效
    GTM_TIM0_CH2_FLT_FE.B.FLT_FE     = FILTER_CON_NUM;//设置滤波计数值
    GTM_TIM0_CH2_FLT_RE.B.FLT_RE     = FILTER_CON_NUM;

    GTM_TIM0_CH0_CTRL.B.TIM_MODE = IfxGtm_Tim_Mode_inputEvent;//输入事件
    GTM_TIM0_CH0_CTRL.B.ISL      = TRUE;//双边沿触发
    GTM_TIM0_CH0_CTRL.B.TIM_EN   = TRUE;//使能
    GTM_TIM0_CH1_CTRL.B.TIM_MODE = IfxGtm_Tim_Mode_inputEvent;
    GTM_TIM0_CH1_CTRL.B.ISL      = TRUE;
    GTM_TIM0_CH1_CTRL.B.TIM_EN   = TRUE;
    GTM_TIM0_CH2_CTRL.B.TIM_MODE = IfxGtm_Tim_Mode_inputEvent;
    GTM_TIM0_CH2_CTRL.B.ISL      = TRUE;
    GTM_TIM0_CH2_CTRL.B.TIM_EN   = TRUE;

    //FSOI引脚，主要用于快速关闭输出
    GTM_TIM1_CH6_CTRL.B.TIM_MODE = IfxGtm_Tim_Mode_inputEvent;
    GTM_TIM1_CH6_CTRL.B.ISL      = TRUE;
    GTM_TIM1_CH6_CTRL.B.TIM_EN   = TRUE;
}

static void spe0_irq_init(void)
{
    IfxSrc_init(&SRC_GTMSPE0IRQ, SRC_CPU0, SRC_SPE_INT_PRIO);//设置SPE的中断优先级 以及处理中断的CPU
    IfxSrc_enable(&SRC_GTMSPE0IRQ);//使能中断
    GTM_SPE0_IRQ_EN.U   = SRC_ENABLE_ALL;//中断使能
    GTM_SPE0_EIRQ_EN.U  = SRC_ENABLE_ALL;//错误中断使能
    GTM_SPE0_IRQ_MODE.U = SRC_PULSE_NOTIFY;//中断类型
}

static void spe0_pattern_init_cfg (void)
{
    spe_g.start_pattern = GTM_SPE0_CTRL_STAT.B.NIP;

    switch (spe_g.start_pattern)
    {
        case INPUT_PAT_0:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_0;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_5;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_0;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_0;
            break;
        case INPUT_PAT_1:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_1;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_0;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_1;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_1;
            break;
        case INPUT_PAT_2:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_2;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_1;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_2;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_2;
            break;
        case INPUT_PAT_3:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_3;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_2;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_3;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_3;
            break;
        case INPUT_PAT_4:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_4;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_3;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_4;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_4;
            break;
        case INPUT_PAT_5:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_5;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_4;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_5;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_5;
            break;
        default:
            break;
    }
    spe_g.pattern_first.Pattern_AIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.AIP;
    spe_g.pattern_first.Pattern_NIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.NIP;
    spe_g.pattern_first.Pattern_PIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.PIP;
    spe_g.pattern_first.Pattern_PTR[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR;
    spe_g.pattern_first.pattern_cnt++;
}

void spe0_pattern_cfg (void)
{
    spe_g.start_pattern = GTM_SPE0_CTRL_STAT.B.NIP;

    switch (spe_g.start_pattern)
    {
        case INPUT_PAT_0:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_0;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_5;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_0;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_0;
            break;
        case INPUT_PAT_1:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_1;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_0;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_1;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_1;
            break;
        case INPUT_PAT_2:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_2;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_1;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_2;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_2;
            break;
        case INPUT_PAT_3:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_3;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_2;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_3;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_3;
            break;
        case INPUT_PAT_4:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_4;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_3;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_4;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_4;
            break;
        case INPUT_PAT_5:
            GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR        = PAT_PTR_5;
            GTM_SPE0_CTRL_STAT.B.PIP                = INPUT_PAT_4;
            GTM_SPE0_CTRL_STAT.B.AIP                = INPUT_PAT_5;
            GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL        = OUTPUT_PAT_5;
            break;
        default:
            break;
    }
}

void spe0_cfg (void)
{
    spe0_irq_init();

    GTM_SPE0_PAT.B.IP0_VAL = VALID_IN_PATTERN;//设置模式有效
    GTM_SPE0_PAT.B.IP1_VAL = VALID_IN_PATTERN;
    GTM_SPE0_PAT.B.IP2_VAL = VALID_IN_PATTERN;
    GTM_SPE0_PAT.B.IP3_VAL = VALID_IN_PATTERN;
    GTM_SPE0_PAT.B.IP4_VAL = VALID_IN_PATTERN;
    GTM_SPE0_PAT.B.IP5_VAL = VALID_IN_PATTERN;
    GTM_SPE0_PAT.B.IP6_VAL = INVALID_IN_PATTERN;//设置模式无效
    GTM_SPE0_PAT.B.IP7_VAL = INVALID_IN_PATTERN;


    GTM_SPE0_PAT.B.IP0_PAT = INPUT_PAT_0;//设置TIMx y z输入的组合值  实际就是霍尔的数值
    GTM_SPE0_PAT.B.IP1_PAT = INPUT_PAT_1;
    GTM_SPE0_PAT.B.IP2_PAT = INPUT_PAT_2;
    GTM_SPE0_PAT.B.IP3_PAT = INPUT_PAT_3;
    GTM_SPE0_PAT.B.IP4_PAT = INPUT_PAT_4;
    GTM_SPE0_PAT.B.IP5_PAT = INPUT_PAT_5;
    GTM_SPE0_PAT.B.IP6_PAT = 0;//无效值
    GTM_SPE0_PAT.B.IP7_PAT = 0;

    now_dir = gpio_get(MOTOR_DIR_IN_PIN);

    if(now_dir){
        GTM_SPE0_OUT_PAT0.U = OUTPUT_PAT_0;//设置当在对应的值达到的时候 控制TOM输出通道的状态
        GTM_SPE0_OUT_PAT1.U = OUTPUT_PAT_1;
        GTM_SPE0_OUT_PAT2.U = OUTPUT_PAT_2;
        GTM_SPE0_OUT_PAT3.U = OUTPUT_PAT_3;
        GTM_SPE0_OUT_PAT4.U = OUTPUT_PAT_4;
        GTM_SPE0_OUT_PAT5.U = OUTPUT_PAT_5;
    }
    else
    {
        GTM_SPE0_OUT_PAT0.U = OUTPUT_PAT_AD_0;//设置当在对应的值达到的时候 控制TOM输出通道的状态
        GTM_SPE0_OUT_PAT1.U = OUTPUT_PAT_AD_1;
        GTM_SPE0_OUT_PAT2.U = OUTPUT_PAT_AD_2;
        GTM_SPE0_OUT_PAT3.U = OUTPUT_PAT_AD_3;
        GTM_SPE0_OUT_PAT4.U = OUTPUT_PAT_AD_4;
        GTM_SPE0_OUT_PAT5.U = OUTPUT_PAT_AD_5;
    }
    GTM_SPE0_OUT_PAT6.U = 0x0;//无效值
    GTM_SPE0_OUT_PAT7.U = 0x0;

    //每换相10次触发中断，主要作用是避免通过一个换相时间去计算速度，从而导致计算的速度是波动的。
    //速度波动是因为霍尔安装误差导致的换相时长是不一样的。
    GTM_SPE0_CMP.B.CMP = REV_CNT_VAL & 0xFFFFFF;
    spe0_pattern_init_cfg();

    GTM_SPE0_CTRL_STAT.B.SPE_EN   = TRUE;
    GTM_SPE0_CTRL_STAT.B.SIE0     = TRUE;//x通道使能
    GTM_SPE0_CTRL_STAT.B.SIE1     = TRUE;//y通道使能
    GTM_SPE0_CTRL_STAT.B.SIE2     = TRUE;//z通道使能
    GTM_SPE0_CTRL_STAT.B.FSOM     = motor_control.brake_flag;  //使能快速关闭模式
    GTM_SPE0_CTRL_STAT.B.PDIR     = 0;
    GTM_SPE0_CTRL_STAT.B.ADIR     = 0;
    GTM_SPE0_CTRL_STAT.B.FSOL     = 0xFF;//关闭的时候8个通道的输出值设定
    GTM_SPE0_CTRL_STAT.B.TRIG_SEL = 2;//使用TOM[i] CH1延时触发换相
    GTM_SPE0_CTRL_STAT.B.TIM_SEL  = 0;//使用TIM0 通道0-通道2作为SPE输入
}

static void spe0_tom_tgc_cfg (void)
{
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL0 = FORCE_UPDATE_EN & 0x3;//强制更新
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL2 = FORCE_UPDATE_EN & 0x3;
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL1 = FORCE_UPDATE_DIS & 0x3;//忽略
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL3 = FORCE_UPDATE_DIS & 0x3;
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL4 = FORCE_UPDATE_DIS & 0x3;
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL5 = FORCE_UPDATE_DIS & 0x3;
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL6 = FORCE_UPDATE_DIS & 0x3;
    GTM_TOM0_TGC0_FUPD_CTRL.B.FUPD_CTRL7 = FORCE_UPDATE_DIS & 0x3;

    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL0 = TOM_OUTPUT_ENABLE & 0x3;//在更新触发的时候上使能输出
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL1 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL2 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL3 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL4 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL5 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL6 = TOM_OUTPUT_ENABLE & 0x3;
    GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL7 = TOM_OUTPUT_ENABLE & 0x3;

    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL0 = ENDIS_EN;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL2 = ENDIS_EN;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL1 = ENDIS_DIS;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL3 = ENDIS_DIS;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL4 = ENDIS_DIS;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL5 = ENDIS_DIS;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL6 = ENDIS_DIS;
    GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL7 = ENDIS_DIS;

    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL0 = ENDIS_EN & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL2 = ENDIS_EN & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL1 = ENDIS_DIS & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL3 = ENDIS_DIS & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL4 = ENDIS_DIS & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL5 = ENDIS_DIS & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL6 = ENDIS_DIS & 0x3;
    GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL7 = ENDIS_DIS & 0x3;
}

void spe0_pwm_tom_cfg (void)
{
    spe0_tom_tgc_cfg ();

    GTM_TOM0_CH2_CTRL.B.OSM        = 1;           //开启关闭one shot mode
    GTM_TOM0_CH2_CTRL.B.SPEM       = 1;
    GTM_TOM0_CH2_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;
    GTM_TOM0_CH2_SR0.U = 10;
    GTM_TOM0_CH2_SR1.U = 10;//设置延迟换相时间

    GTM_TOM0_CH0_CTRL.B.SL         = TOM_CH_SL_HIGH;     //占空比表示的是高电平的占比
    GTM_TOM0_CH0_CTRL.B.OSM        = OSM_DIS;           //关闭one shot mode
    GTM_TOM0_CH0_CTRL.B.SPEM       = SPEM_DIS;
    GTM_TOM0_CH0_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH0_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;
    GTM_TOM0_CH0_CTRL.B.RST_CCU0   = CM0_MATCHING;

    spe_g.speed.duty   = 100;
    spe_g.speed.period = 5000;

    GTM_TOM0_CH0_SR1.U = spe_g.speed.duty;
    GTM_TOM0_CH0_SR0.U = spe_g.speed.period;

    GTM_TOM0_CH1_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH1_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH1_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH1_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    GTM_TOM0_CH3_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH3_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH3_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH3_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    GTM_TOM0_CH4_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH4_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH4_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH4_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    GTM_TOM0_CH5_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH5_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH5_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH5_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    GTM_TOM0_CH6_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH6_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH6_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH6_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    GTM_TOM0_CH7_CTRL.B.OSM        = OSM_DIS;
    GTM_TOM0_CH7_CTRL.B.SPEM       = SPEM_EN;
    GTM_TOM0_CH7_CTRL.B.TRIGOUT    = TRIG_CCU0;
    GTM_TOM0_CH7_CTRL.B.CLK_SRC_SR = TOM_CH_CMU_FXCLK0;

    /* TOM Global channel control mechanism */
    GTM_TOM0_TGC0_GLB_CTRL.B.HOST_TRIG = 1;//全局触发请求
}
//选择输出引脚
void spe0_pwm_mux_outport_cfg(void)
{
    IfxGtm_PinMap_setTomTout(&PWM_PRODUCE_PIN,  IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&A_PHASE_PIN_H,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&A_PHASE_PIN_L,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&B_PHASE_PIN_H,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&B_PHASE_PIN_L,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&C_PHASE_PIN_H,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    IfxGtm_PinMap_setTomTout(&C_PHASE_PIN_L,    IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
}
//中断触发检测
void pattern_chk(void)
{
    uint8 aip, nip, pip, ptr;

    aip = spe_g.pattern.Pattern_AIP[spe_g.pattern.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.AIP;
    nip = spe_g.pattern.Pattern_NIP[spe_g.pattern.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.NIP;
    pip = spe_g.pattern.Pattern_PIP[spe_g.pattern.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.PIP;
    ptr = spe_g.pattern.Pattern_PTR[spe_g.pattern.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR;
    spe_g.pattern.pattern_cnt++;
    if (spe_g.pattern.pattern_cnt >= MAX_PATTERN)
    {
        spe_g.pattern.pattern_cnt = 0;
    }

    if (spe_g.pattern_first.pattern_cnt < MAX_PATTERN)
    {
        spe_g.pattern_first.Pattern_AIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.AIP;
        spe_g.pattern_first.Pattern_NIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.NIP;
        spe_g.pattern_first.Pattern_PIP[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.PIP;
        spe_g.pattern_first.Pattern_PTR[spe_g.pattern_first.pattern_cnt] = GTM_SPE0_CTRL_STAT.B.SPE_PAT_PTR;
        spe_g.pattern_first.pattern_cnt++;
    }
}

void gtm_bldc_init(void)
{
    clock_cfg();    //GTM模块时钟配置初始化
    ccm_cfg();      //GTM中断刷新
    hall_tim_input_cfg();      //初始化霍尔捕获以及刹车捕获
    spe0_cfg();                //初始化SPE0模块通道
    spe0_pwm_tom_cfg();        //初始化TOM0输出模块
    spe0_pwm_mux_outport_cfg();//选择最终输出PWM的引脚
}
