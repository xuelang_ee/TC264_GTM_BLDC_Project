#ifndef _GPIO_H
#define _GPIO_H

#include "common.h"




#define LED_ERR_PIN         P11_2
#define LED_RUN_PIN         P11_3
#define LED_MODEL_PIN       P11_6
#define EN_PIN              P20_9
#define EN_FSOI_PIN            P20_10


void led_init(void);
void pre_driver_init(void);
void pre_driver_start(void);
void led_output(void);

#endif
