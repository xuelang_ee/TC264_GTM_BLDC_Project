/*
 * motor.h
 *
 *  Created on: 2022年4月7日
 *      Author: XXWY
 */

#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "zf_gpio.h"
#include "IFXGTM_TIM_IN.h"
#include "ifxGtm_Tim.h"
#include "gtm_pwm.h"
#include "pwm_input.h"
#include "common.h"

#define MOTOR_SPEED_OUT_PIN ATOM1_CH2_P21_4 //电机速度输出引脚
#define MOTOR_DIR_OUT_PIN   P21_5          //电机运行方向输出引脚


#define COMMUTATION_TIMEOUT 5000

#define MOTOR_RUN   1
#define MOTOR_STOP  0

//定义电机极对数
#if MOTOR_TYPE == 0
#define POLEPAIRS           1
#elif MOTOR_TYPE == 1
#define POLEPAIRS           7
#endif

#define PWM_PIT_NUM          60*FPWM

typedef enum
{
    FORWARD,    //正转
    REVERSE,    //反转
}MOTOR_DIR_enum;

typedef enum
{
    MOTOR_DISABLE,  //驱动关闭
    MOTOR_ENABLE,   //驱动使能
}MOTOR_EN_STATUS_enum;

typedef struct
{
    MOTOR_EN_STATUS_enum en_status; //指示电机使能状态
    uint8 brake_flag;   //指示当前刹车是否有效    1：正在刹车  0：正常运行
    MOTOR_DIR_enum  dir;//电机旋转方向 FORWARD：正转  REVERSE：反转     BRAKE：刹车
    int32 set_speed;    //设置的速度
    int32 max_speed;    //速度最大值
    int32 min_speed;    //速度最小值
}motor_struct;

extern motor_struct motor_control;

extern uint8 now_dir;
extern uint8 phase_change;
extern uint8 motor_state;

void motor_information_out_init(void);
void motor_set_dir(void);
void speed_ctrl(speed_st speed);
void advance_switch(void);
void phase_change_manage(void);
void phase_change_time_check(void);
void motor_dir_out(void);
void motor_speed_out(void);
void motor_init(void);

#endif /* CODE_MOTOR_H_ */
