#ifndef _spe_h
#define _spe_h

#include "IFXGTM_TIM_IN.h"
#include "ifxGtm_Tim.h"
#include "bldc_config.h"
#include "common.h"

#define MAX_PATTERN 61

#define CMU_CLK_ALL_DISABLE 0x55555555
#define CMU_CLK_ENABLE  2

#define VALID_IN_PATTERN    (1u)
#define INVALID_IN_PATTERN  (0u)

//霍尔硬件顺序为2 3 1 5 4 6
#define INPUT_PAT_0 (5u)
#define INPUT_PAT_1 (1u)
#define INPUT_PAT_2 (3u)
#define INPUT_PAT_3 (2u)
#define INPUT_PAT_4 (6u)
#define INPUT_PAT_5 (4u)
#if MOTOR_TYPE == 0
//电机正转
#define OUTPUT_PAT_0 0xEA8A   //A上桥C下桥开启，其他关闭
#define OUTPUT_PAT_1 0xE8AA   //B上桥C下桥开启，其他关闭
#define OUTPUT_PAT_2 0xA8EA   //B上桥A下桥开启，其他关闭
#define OUTPUT_PAT_3 0x8AEA   //C上桥A下桥开启，其他关闭
#define OUTPUT_PAT_4 0x8EAA   //C上桥B下桥开启，其他关闭
#define OUTPUT_PAT_5 0xAE8A   //A上桥B下桥开启，其他关闭
//电机反转
#define OUTPUT_PAT_AD_0 0xA8EA //B上桥A下桥开启，其他关闭
#define OUTPUT_PAT_AD_1 0x8AEA //C上桥A下桥开启，其他关闭
#define OUTPUT_PAT_AD_2 0x8EAA //C上桥B下桥开启，其他关闭
#define OUTPUT_PAT_AD_3 0xAE8A //A上桥B下桥开启，其他关闭
#define OUTPUT_PAT_AD_4 0xEA8A //A上桥C下桥开启，其他关闭
#define OUTPUT_PAT_AD_5 0xE8AA //B上桥C下桥开启，其他关闭
#elif MOTOR_TYPE == 1
//电机正转

#define OUTPUT_PAT_0 0xAE8A   //A上桥B下桥开启，其他关闭   2 0xAE8A
#define OUTPUT_PAT_1 0x8EAA   //C上桥B下桥开启，其他关闭   6 0x8EAA
#define OUTPUT_PAT_2 0x8AEA   //C上桥A下桥开启，其他关闭   4 0x8AEA
#define OUTPUT_PAT_3 0xA8EA   //B上桥A下桥开启，其他关闭   5 0xA8EA
#define OUTPUT_PAT_4 0xE8AA   //B上桥C下桥开启，其他关闭   1 0xE8AA
#define OUTPUT_PAT_5 0xEA8A   //A上桥C下桥开启，其他关闭   3 0xEA8A
//电机反转
#define OUTPUT_PAT_AD_0 0x8EAA   //C上桥B下桥开启，其他关闭   6 0x8EAA
#define OUTPUT_PAT_AD_1 0x8AEA   //C上桥A下桥开启，其他关闭   4 0x8AEA
#define OUTPUT_PAT_AD_2 0xA8EA   //B上桥A下桥开启，其他关闭   5 0xA8EA
#define OUTPUT_PAT_AD_3 0xE8AA   //B上桥C下桥开启，其他关闭   1 0xE8AA
#define OUTPUT_PAT_AD_4 0xEA8A   //A上桥C下桥开启，其他关闭   3 0xEA8A
#define OUTPUT_PAT_AD_5 0xAE8A   //A上桥B下桥开启，其他关闭   2 0xAE8A

#endif

#define OUTPUT_PAT_FSOI 0xEEEA      //刹车


#define REV_CNT_VAL         (10u)   //换相固定次数触发中断
#define FILTER_CON_NUM      (10u)   //霍尔信号的滤波市场，主要作用是去除毛刺

#define FORCE_UPDATE_EN     (2)     //输出控制寄存器强制更新
#define FORCE_UPDATE_DIS    (1)     //忽略更新

#define TOM_OUTPUT_ENABLE   (2)     //输出通道使能
#define TOM_OUTPUT_DISABLE  (1)     //输出通道失能

#define ENDIS_EN            (2)     //更新使能
#define ENDIS_DIS           (1)     //更新失能


#define TOM_CH_SL_LOW           (0)
#define TOM_CH_SL_HIGH          (1)
#define OSM_EN                  (1)
#define OSM_DIS                 (0)
#define SPEM_EN                 (1)
#define SPEM_DIS                (0)
#define CM0_MATCHING            (0)
#define TRIG_0_MATCHING         (1)
#define TRIG_0_EXT_TRIG         (0)


#define TRIG_CCU0               (1)
#define TRIG_BEFORE             (0)

#define TOM_CH_CMU_FXCLK0       (0)

#define SRC_SPE_INT_PRIO    (90)
#define SRC_PULSE_NOTIFY    2U
#define SRC_ENABLE_ALL      0x1F
#define SRC_CPU0            0
#define SRC_ENABLE          1

#define PAT_PTR_0 (0u)
#define PAT_PTR_1 (1u)
#define PAT_PTR_2 (2u)
#define PAT_PTR_3 (3u)
#define PAT_PTR_4 (4u)
#define PAT_PTR_5 (5u)


#define FCY             ((uint32)100000000)     //系统时钟
#define FPWM            ((uint16)20000)         //PWM频率
#define PWM_PRIOD_LOAD  (uint16)(5000)          //PWM周期装载值


//以下引脚为实际输入输出引脚
#define HALL_A          IfxGtm_TIM0_2_TIN33_P33_11_IN   //霍尔信号A相输入
#define HALL_B          IfxGtm_TIM0_0_TIN34_P33_12_IN   //霍尔信号B相输入
#define HALL_C          IfxGtm_TIM0_1_TIN35_P33_13_IN   //霍尔信号C相输入

#define FSOI_PIN        IfxGtm_TIM1_6_TIN66_P20_10_IN   //刹车信号输入（快速关闭）

#define PWM_PRODUCE_PIN IfxGtm_TOM0_0_TOUT26_P33_4_OUT  //PWM生成引脚
#define A_PHASE_PIN_H   IfxGtm_TOM0_2_TOUT28_P33_6_OUT  //电机A相上桥控制引脚
#define A_PHASE_PIN_L   IfxGtm_TOM0_3_TOUT29_P33_7_OUT  //电机A相下桥控制引脚
#define B_PHASE_PIN_H   IfxGtm_TOM0_4_TOUT30_P33_8_OUT  //电机B相上桥控制引脚
#define B_PHASE_PIN_L   IfxGtm_TOM0_5_TOUT40_P32_4_OUT  //电机B相下桥控制引脚
#define C_PHASE_PIN_H   IfxGtm_TOM0_6_TOUT42_P23_1_OUT  //电机B相上桥控制引脚
#define C_PHASE_PIN_L   IfxGtm_TOM0_7_TOUT64_P20_8_OUT  //电机B相下桥控制引脚


typedef struct
{
    uint32 period;
    uint32 duty;
} speed_st;

typedef struct
{
    uint8 Pattern_AIP[MAX_PATTERN];
    uint8 Pattern_NIP[MAX_PATTERN];
    uint8 Pattern_PIP[MAX_PATTERN];
    uint8 Pattern_PTR[MAX_PATTERN];
    uint8 pattern_cnt;
} pattern_st;

typedef struct
{
    uint32 spe_rcmp; /* SPE revolution counter match event */
    uint32 spe_dchg; /* SPE_DIR bit changed on behalf of new input pattern */
    uint32 spe_perr; /* Wrong or invalid pattern detected at input */
    uint32 spe_bis;  /* Bouncing input signal detected */
    uint32 spe_nipd; /* New input pattern interrupt occurred */
} irq_st;

typedef struct
{
    speed_st    speed;
    pattern_st  pattern;
    pattern_st  pattern_first;
    irq_st      irqcnt;
    uint8       start_pattern;
    uint8       hall_now;
} spe_st;


extern spe_st spe_g;

void spe0_pattern_cfg (void);
void gtm_bldc_init(void);
void pattern_chk(void);



#endif

