/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/


#include "ifxGtm_Tim.h"
#include "gtm_pwm.h"
#include "pwm_input.h"
#include "motor.h"
#include "gpio.h"
#include "adc.h"
#include "move_filter.h"
#include "key.h"
#include "pid.h"
#include "headfile.h"
#pragma section all "cpu0_dsram"
//将本语句与#pragma section all restore语句之间的全局变量都放在CPU0的RAM中

int core0_main(void)
{
	get_clk();//获取时钟频率  务必保留
	//用户在此处调用各种初始化函数等

	led_init(); //初始化LED引脚

	key_init(); //初始化按键引脚

    adc_collection_init(); //AD采值引脚初始化

	move_filter_init(&speed_filter); //初始化速度平滑滤波

	motor_information_out_init(); //初始化运行信息输出端口

	pwm_input_init(); //初始化输入捕获

	motor_init(); //电机初始参数配置

    closed_loop_pi_init(); //PID参数初始化

	gtm_bldc_init(); //初始化GTM模块

	pit_interrupt_ms(CCU6_1, PIT_CH1, 5); //周期中断初始化

    //等待所有核心初始化完毕
	IfxCpu_emitEvent(&g_cpuSyncEvent);
	IfxCpu_waitEvent(&g_cpuSyncEvent, 0xFFFF);
	enableInterrupts();

	while (TRUE)
	{
		//用户在此处编写任务代码

	    led_output();       //根据当前状态点亮或者熄灭LED灯

        data_conversion((int16)myabs(speed_filter.data_average/10), (int16)spe_g.speed.duty, (int16)adc_information.current_board, (int16)spe_g.speed.duty, virtual_scope_data);
        uart_putbuff(UART_0, virtual_scope_data, sizeof(virtual_scope_data));  //数据转换完成后，使用串口发送将数组的内容发送出去

	}
}

#pragma section all restore


