
 
/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		isr
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		ADS v1.2.2
 * @Target core		TC264D
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-3-23
 ********************************************************************************************************************/


#include <gtm_pwm.h>
#include "isr_config.h"
#include "IFXGTM_TIM_IN.h"
#include "pwm_input.h"
#include "bldc_config.h"
#include "move_filter.h"
#include "gtm_pwm.h"
#include "motor.h"
#include "key.h"
#include "adc.h"
#include "pid.h"
#include "isr.h"

//PIT中断函数  示例
IFX_INTERRUPT(cc60_pit_ch0_isr, 0, CCU6_0_CH0_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_0, PIT_CH0);

}


IFX_INTERRUPT(cc60_pit_ch1_isr, 0, CCU6_0_CH1_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_0, PIT_CH1);

}

IFX_INTERRUPT(cc61_pit_ch0_isr, 0, CCU6_1_CH0_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_1, PIT_CH0);

}

IFX_INTERRUPT(cc61_pit_ch1_isr, 0, CCU6_1_CH1_ISR_PRIORITY)
{
	enableInterrupts();//开启中断嵌套
	PIT_CLEAR_FLAG(CCU6_1, PIT_CH1);

	adc_read();

	key_scan();     //按键扫描

	motor_set_dir();

	motor_speed_out();

	phase_change_time_check();

    #if BLDC_CLOSE_LOOP_ENABLE
    //根据接收到的信号，去计算出需要设置的速度值
        if(model_state) motor_control.set_speed = motor_control.max_speed * adc_information.current_board/4096;
        else motor_control.set_speed = motor_control.max_speed * pwm_in_duty/PWM_PRIOD_LOAD;
        //进行PI闭环计算
        if(motor_control.dir == FORWARD)
            spe_g.speed.duty = (int16)closed_loop_pi_calc((float)(motor_control.set_speed - speed_filter.data_average));
        else
            spe_g.speed.duty = (int16)closed_loop_pi_calc((float)(motor_control.set_speed + speed_filter.data_average));
    #else
        if(model_state) spe_g.speed.duty = adc_information.current_board*PWM_PRIOD_LOAD/4096;
        else spe_g.speed.duty = pwm_in_duty;
    #endif

    speed_ctrl(spe_g.speed);
}

int8 new_data_false = 0;
IFX_INTERRUPT(gtm_pwm_in, 0, GTM_PWM_IN_PRIORITY)
{
    IfxGtm_Tim_In_update(&driver);

    if(FALSE == driver.newData)
    {
        if(gpio_get(P20_14))
        {
            if(new_data_false > 0)
            {
                new_data_false --;
            }
            else
            {
                driver.periodTick = 20000;
                driver.pulseLengthTick = driver.periodTick;
            }
        }
        else
        {
            new_data_false = 3;
            driver.periodTick = 20000;
            driver.pulseLengthTick = 0;
        }
    }
    else
    {
        new_data_false = 0;
    }
    pwm_in_duty = (uint16)limit_ab((driver.pulseLengthTick * 2500 / driver.periodTick), 0, 2500) * 2;
}

IFX_INTERRUPT (spe_isr, 0, SRC_SPE_INT_PRIO)
{
    pattern_chk();
    if (GTM_SPE0_IRQ_NOTIFY.B.SPE_RCMP)         //读值正常读取单片机寄存器即可
    {
        /* revolution counter match event */    //旋转次数匹配中断
        GTM_SPE0_IRQ_NOTIFY.B.SPE_RCMP = 1;     //写入1清除寄存器内容
        /* Set the new match, GTM will generate ther interrupt every REV_CNT_VAL revolution */
        GTM_SPE0_CMP.B.CMP = (GTM_SPE0_CMP.B.CMP + REV_CNT_VAL) & 0xFFFFFF;
        spe_g.irqcnt.spe_rcmp++ ;
    }

    if (GTM_SPE0_IRQ_NOTIFY.B.SPE_DCHG)
    {
        /* Direction Changed */                 //电机旋转方向变化
        GTM_SPE0_IRQ_NOTIFY.B.SPE_DCHG = 1;
        spe_g.irqcnt.spe_dchg++;
        motor_dir_out();
    }

    if (GTM_SPE0_IRQ_NOTIFY.B.SPE_PERR)
    {
        /* SPE Invalid input pattern detected *///霍尔输入无效
        GTM_SPE0_IRQ_NOTIFY.B.SPE_PERR = 1;
        spe_g.irqcnt.spe_perr++;
        spe0_pattern_cfg();
    }

    if (GTM_SPE0_IRQ_NOTIFY.B.SPE_BIS)
    {
        /* Bouncing input signal detected */    //检测到输入信号反弹  跳跃信号 一次中断出现多次霍尔值改变
        GTM_SPE0_IRQ_NOTIFY.B.SPE_BIS = 1;
        spe_g.irqcnt.spe_bis++;
    }

    if (GTM_SPE0_IRQ_NOTIFY.B.SPE_NIPD)
    {
        /* New Input Pattern occurred */        //新的且正确的输入模式（换相）
        GTM_SPE0_IRQ_NOTIFY.B.SPE_NIPD = 1;
        spe_g.irqcnt.spe_nipd++;
        spe_g.hall_now = GTM_SPE0_CTRL_STAT.B.NIP;
        phase_change_manage();
    }
#if BLDC_BRAKE_ENABLE == 1
    if(spe_g.speed.duty == 0 || (!now_dir != motor_control.dir))
    {
        GTM_SPE0_OUT_CTRL.B.SPE_OUT_CTRL = OUTPUT_PAT_FSOI;
    }
#endif
}


IFX_INTERRUPT(eru_ch0_ch4_isr, 0, ERU_CH0_CH4_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH0_REQ4_P10_7))//通道0中断
	{
		CLEAR_GPIO_FLAG(ERU_CH0_REQ4_P10_7);
	}

	if(GET_GPIO_FLAG(ERU_CH4_REQ13_P15_5))//通道4中断
	{
		CLEAR_GPIO_FLAG(ERU_CH4_REQ13_P15_5);
	}
}

IFX_INTERRUPT(eru_ch1_ch5_isr, 0, ERU_CH1_CH5_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH1_REQ5_P10_8))//通道1中断
	{
		CLEAR_GPIO_FLAG(ERU_CH1_REQ5_P10_8);
	}

	if(GET_GPIO_FLAG(ERU_CH5_REQ1_P15_8))//通道5中断
	{
		CLEAR_GPIO_FLAG(ERU_CH5_REQ1_P15_8);
	}
}

//由于摄像头pclk引脚默认占用了 2通道，用于触发DMA，因此这里不再定义中断函数
//IFX_INTERRUPT(eru_ch2_ch6_isr, 0, ERU_CH2_CH6_INT_PRIO)
//{
//	enableInterrupts();//开启中断嵌套
//	if(GET_GPIO_FLAG(ERU_CH2_REQ7_P00_4))//通道2中断
//	{
//		CLEAR_GPIO_FLAG(ERU_CH2_REQ7_P00_4);
//
//	}
//	if(GET_GPIO_FLAG(ERU_CH6_REQ9_P20_0))//通道6中断
//	{
//		CLEAR_GPIO_FLAG(ERU_CH6_REQ9_P20_0);
//
//	}
//}



IFX_INTERRUPT(eru_ch3_ch7_isr, 0, ERU_CH3_CH7_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
	if(GET_GPIO_FLAG(ERU_CH3_REQ6_P02_0))//通道3中断
	{
		CLEAR_GPIO_FLAG(ERU_CH3_REQ6_P02_0);
		if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_vsync();
		else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_vsync();
		else if	(CAMERA_BIN       == camera_type)	ov7725_vsync();

	}
	if(GET_GPIO_FLAG(ERU_CH7_REQ16_P15_1))//通道7中断
	{
		CLEAR_GPIO_FLAG(ERU_CH7_REQ16_P15_1);

	}
}



IFX_INTERRUPT(dma_ch5_isr, 0, ERU_DMA_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套

	if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_dma();
	else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_dma();
	else if	(CAMERA_BIN       == camera_type)	ov7725_dma();
}


//串口中断函数  示例
IFX_INTERRUPT(uart0_tx_isr, 0, UART0_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart0_handle);
}
IFX_INTERRUPT(uart0_rx_isr, 0, UART0_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart0_handle);
}
IFX_INTERRUPT(uart0_er_isr, 0, UART0_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart0_handle);
}

//串口1默认连接到摄像头配置串口
IFX_INTERRUPT(uart1_tx_isr, 0, UART1_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart1_handle);
}
IFX_INTERRUPT(uart1_rx_isr, 0, UART1_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart1_handle);
    if		(CAMERA_GRAYSCALE == camera_type)	mt9v03x_uart_callback();
    else if (CAMERA_BIN_UART  == camera_type)	ov7725_uart_callback();
}
IFX_INTERRUPT(uart1_er_isr, 0, UART1_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart1_handle);
}


//串口2默认连接到无线转串口模块
IFX_INTERRUPT(uart2_tx_isr, 0, UART2_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart2_handle);
}
IFX_INTERRUPT(uart2_rx_isr, 0, UART2_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart2_handle);
    switch(wireless_type)
    {
    	case WIRELESS_SI24R1:
    	{
    		wireless_uart_callback();
    	}break;

    	case WIRELESS_CH9141:
		{
		    bluetooth_ch9141_uart_callback();
		}break;
    	default:break;
    }

}
IFX_INTERRUPT(uart2_er_isr, 0, UART2_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart2_handle);
}



IFX_INTERRUPT(uart3_tx_isr, 0, UART3_TX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrTransmit(&uart3_handle);
}
IFX_INTERRUPT(uart3_rx_isr, 0, UART3_RX_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrReceive(&uart3_handle);
}
IFX_INTERRUPT(uart3_er_isr, 0, UART3_ER_INT_PRIO)
{
	enableInterrupts();//开启中断嵌套
    IfxAsclin_Asc_isrError(&uart3_handle);
}
